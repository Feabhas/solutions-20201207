// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm_filter.h"
#include "Circular_buffer.h"
#include "Display.h"
#include "Generator.h"

int main()
{
  Buffer       pipe1{};
  Buffer       pipe2{};
  Generator    generator{ pipe1 }; // associate by construction
  Alarm_filter filter{ Alarm::Type::warning, pipe1, pipe2 };
  // Generator generator{ };
  Display display{ pipe2 };

  // generator.associate(pipe1);  // associate by member function
  // associate(generator, pipe1); // associate by free (friend) function

  for (auto i = 0; i < 2; ++i) {
    generator.execute();
    filter.execute();
    display.execute();
  }
}

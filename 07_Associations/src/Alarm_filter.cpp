// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm_filter.h"
#include "Circular_buffer.h"
#include <algorithm>
#include <cassert>
#include <iostream>

using namespace std;

Alarm_filter::Alarm_filter(Alarm::Type remove_this) : value{ remove_this } {}
Alarm_filter::Alarm_filter(Alarm::Type remove_this, Buffer& in, Buffer& out) :
  value{ remove_this }, input{ &in }, output{ &out }
{
}

void Alarm_filter::execute()
{
  assert(input);
  assert(output);

  cout << "ALARM FILTER : -------------------------------\n";
  unsigned count{};
  while (auto a_ = input->get()) {
    auto& alarm = *a_; // if not std::nullopt then create local binding
    if (alarm.type() != this->value) { output->add(alarm); }
    else {
      ++count;
    }
  }
  if (count > 0) {
    std::cout << "Filtered " << count << " alarm";
    std::cout << ((count != 1) ? "s" : "") << "\n\n";
  }
}

void connect(Alarm_filter& filter, Buffer& in, Buffer& out)
{
  filter.input  = &in;
  filter.output = &out;
}
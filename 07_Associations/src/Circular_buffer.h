// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#ifndef BUFFER_H
#define BUFFER_H

#include "Alarm.h"
#include <array>
#include <cstddef>
#include <optional>

class Buffer {
public:
  Buffer()          = default;
  using buffer_type = Alarm;

  bool                       add(buffer_type const& in);
  std::optional<buffer_type> get();
  bool                       is_empty() const;
  constexpr std::size_t      capacity() const { return sz; }
  void                       reset();

private:
  static constexpr std::size_t sz{ 16 };
  using Container = std::array<buffer_type, sz>;

  Container   elems{};
  std::size_t write_pos{};
  std::size_t read_pos{};
  std::size_t num_elems{};
};

#endif

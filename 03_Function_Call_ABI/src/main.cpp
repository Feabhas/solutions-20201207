// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm.h"
#include <iostream>
using namespace std;

int main()
{
  Alarm a1;                              //
  Alarm a2{};                            // Value-initialised
  Alarm a3{ Alarm::warning };            // Explicitly initialised
  auto  a4 = make_alarm(Alarm::caution); // factory-function

  // cout << static_cast<int>(a1.type()) << '\n';
  // cout << a2.as_string() << '\n';
  // cout << a1 << '\n';
  // cout << a2 << '\n';
  // cout << a3 << '\n';
  // cout << a4 << '\n';
  a1 = make_alarm(Alarm::caution); // RVO
  print_alarm(a4);
}

# Running solutions

To execute any of the example solutions, within the exercise directory run:
```
$ bash run.sh
```
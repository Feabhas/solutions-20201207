// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Generator.h"
#include "Circular_buffer.h"

#include <cassert>
#include <iostream>
#include <string_view>
using namespace std;

namespace
{
  Alarm::Type random_alarm()
  {
    return (static_cast<Alarm::Type>((rand() % 3) + 1));
  }

  constexpr unsigned num_of_strings{ 6 };

  constexpr std::array<std::string_view, num_of_strings> alarm_strings{
    "Panic!                                                 *",
    "Run away!                                              *",
    "Ignore this alarm.                                     *",
    "Oooops!                                                *",
    "Things are going horribly wrong.                       *",
    "Please fix immediately.                                *"
  };

  std::string_view random_string()
  {
    return (alarm_strings[rand() % num_of_strings]);
  }

} // namespace

void Generator::execute()
{
  // assert(output);
  cout << "GENERATOR : ----------------------------------\n";

  // auto num_to_add = rand() % 16;
  auto num_to_add = 1;
  for (auto i = 0; i < num_to_add; ++i) {
    Alarm alarm{ random_alarm(), random_string() }; // (4)
    cout << ">>> sending : " << alarm.as_string() << '\n';
    output->add(std::move(alarm));
    // output->emplace(random_alarm(), random_string());
  }
  cout << '\n';
}

Generator::Generator(Buffer& pipe) : output{ &pipe } {}

// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm.h"
#include <array>

constexpr std::array<std::string_view, 4> lookup_str{ "invalid",
                                                      "advisory",
                                                      "caution",
                                                      "warning" };
Alarm::Alarm()
{
  // std::clog << "Alarm default ctor\n";
}

Alarm::Alarm(Type alarm_init) : value{ alarm_init }
{
  std::clog << "Alarm non-default ctor: " << static_cast<int>(value) << " "
            << as_string() << "\n";
}

Alarm::Alarm(Type alarm_init, std::string_view str) :
  value{ alarm_init }, description{ str }
{
  std::clog << "Alarm non-default ctor with string: " << static_cast<int>(value)
            << " " << as_string() << " ";
  std::clog << "\n";
}

Alarm::~Alarm()
{
  // std::clog << "Alarm dtor\n";
}

void swap(Alarm& lhs, Alarm& rhs)
{
  using std::swap;
  swap(lhs.value, rhs.value);
  swap(lhs.description, rhs.description);
}

Alarm::Alarm(Alarm&& rhs) noexcept : Alarm{}
{
  std::clog << "Alarm move ctor\n";
  swap(*this, rhs);
}

Alarm& Alarm::operator=(Alarm&& rhs) noexcept
{
  std::clog << "Alarm move op=\n";
  if (this != &rhs) {
    value       = std::exchange(rhs.value, Type::invalid);
    description = std::exchange(rhs.description, "");
  }
  return *this;
}

std::string_view Alarm::as_string() const
{
  return lookup_str[static_cast<int>(value)];
}

std::string_view Alarm::what() const
{
  return description;
}

Alarm::Type Alarm::type() const
{
  return value;
}

std::ostream& operator<<(std::ostream& os, Alarm const& alarm)
{
  os << alarm.as_string() << " - " << alarm.what();
  return os;
}

Alarm::Alarm(Alarm const& rhs) : Alarm{ rhs.value, rhs.description }
{
  std::clog << "Alarm copy ctor\n";
}

Alarm& Alarm::operator=(Alarm const& rhs) // swap-copy idiom
{
  // rhs is a copy of a1;
  std::clog << "Alarm copy op=\n";
  // swap(a2, rhs);
  auto local{ rhs };
  swap(*this, local);
  // return a2;
  return *this;
}
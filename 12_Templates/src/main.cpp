// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

// #include "Buffer.h"
#include "Display.h"
#include "Generator.h"
#include "Pipeline.h"
#include "Pipeline_buffer.h"

#include <charconv>

int main(int argc, char** argv)
{
  int run_count{ 1 };

  if (argc > 1) {
    std::string_view run_count_str{ argv[1] };
    // run_count is unmodified if the conversion fails
    if (auto [p, ec] =
          std::from_chars(run_count_str.data(),
                          run_count_str.data() + run_count_str.size(),
                          run_count);
        ec != std::errc()) {
      std::cerr << "Parameter should be an integral value\n";
    }
  }

  std::cout << "Running for : " << run_count << " cycles"
            << "\n";

  Buffer pipe{}; // using Buffer = tBuffer<Alarm,16>;

  Generator generator{ pipe };
  Display   display{ pipe };

  Pipeline pipeline{};
  pipeline.add(generator);
  pipeline.add(display);
  for (int i = 0; i < run_count; ++i) {
    pipeline.run();
  }
}

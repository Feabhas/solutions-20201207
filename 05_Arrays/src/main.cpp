// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm.h"
#include "Circular_buffer.h"
#include <cassert>
#include <iostream>
using namespace std;

int main()
{
  Alarm a1;                         //
  Alarm a2{};                       // Value-initialised
  Alarm a3{ Alarm::Type::warning }; // Explicitly initialised

  Buffer pipe{};

  { // Test initial condition
    assert(pipe.is_empty() == true);
  }
  { // Test read from empty
    auto elem = pipe.get();
    assert(elem == std::nullopt);
  }
  { // Test add one
    auto ok = pipe.add(a3);
    assert(ok == true);
    assert(pipe.is_empty() == false);
  }
  {                               // Test get one
    if (auto elem = pipe.get()) { // C++17
      std::cout << elem->as_string() << '\n';
      assert(elem->type() == a3.type());
    }
    assert(pipe.is_empty() == true);
  }

  { // Test add to full
    for (std::size_t count = 0; count < pipe.capacity(); ++count) {
      auto ok = pipe.add(Alarm{ static_cast<Alarm::Type>((rand() % 3) + 1) });
      assert(ok == true);
    }
    auto ok = pipe.add(Alarm{});
    assert(ok == false);
  }
  { // Test wrap-around behaviour
    // Read 1/2
    for (std::size_t count = 0; count < (pipe.capacity() / 2); ++count) {
      if (auto elem = pipe.get()) { std::cout << elem->as_string() << ' '; }
    }
    std::cout << '\n';

    // write to full gain
    bool ok{};
    do {
      ok = pipe.add(Alarm{ static_cast<Alarm::Type>((rand() % 3) + 1) });
    } while (ok);

    // read to empty
    decltype(pipe.get()) elem{}; // use decltype to derive std::optional
    do {
      elem = pipe.get();
      if (elem) std::cout << elem->as_string() << ' ';
    } while (elem);
    std::cout << '\n';
  }
  { // test to clear array
    for (std::size_t count = 0; count < pipe.capacity(); ++count) {
      auto ok = pipe.add(Alarm{ static_cast<Alarm::Type>((rand() % 3) + 1) });
    }
    assert(pipe.is_empty() == false);
    pipe.reset();
    assert(pipe.is_empty() == true);
  }
}

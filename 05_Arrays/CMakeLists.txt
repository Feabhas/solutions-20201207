cmake_minimum_required(VERSION 3.2)

project(Arm-advanced-cpp C CXX)
set(CMAKE_CXX_STANDARD 17)

add_compile_options(
    -Wall
    -Wextra
)

# tell CMake to use CTest extension
enable_testing()

include_directories(src)

add_subdirectory(src)

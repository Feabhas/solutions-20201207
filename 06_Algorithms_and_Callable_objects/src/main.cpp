// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm.h"
#include "Circular_buffer.h"
#include <algorithm>
#include <cassert>
#include <iostream>
using namespace std;

int main()
{
  Alarm a1;                         //
  Alarm a2{};                       // Value-initialised
  Alarm a3{ Alarm::Type::warning }; // Explicitly initialised

  static constexpr std::size_t sz{ 16 };
  std::array<Alarm, sz>        local{
    Alarm{ Alarm::Type::caution },  Alarm{ Alarm::Type::caution },
    Alarm{ Alarm::Type::advisory }, Alarm{ Alarm::Type::caution },
    Alarm{ Alarm::Type::warning }, // (1)
    Alarm{ Alarm::Type::caution },  Alarm{ Alarm::Type::caution },
    Alarm{ Alarm::Type::advisory }, Alarm{ Alarm::Type::advisory },
    Alarm{ Alarm::Type::caution },  Alarm{ Alarm::Type::warning }, // (2)
    Alarm{ Alarm::Type::caution },  Alarm{ Alarm::Type::warning }, // (3)
    Alarm{ Alarm::Type::caution },  Alarm{ Alarm::Type::warning }, // (4)
    Alarm{ Alarm::Type::caution }
  };

  // { // Populate array with random alarms
  //   for (std::size_t count = 0; count < local.size(); ++count) {
  //     local[count] = Alarm{ Alarm::Type((rand() % 3) + 1) };
  //   }
  // }
  for (auto& e : local) {
    if (e.type() != Alarm::Type::invalid) { std::cout << e.as_string() << ' '; }
  }

  std::cout << std::endl;

  constexpr auto filter_value{ Alarm::Type::warning };

  auto type_compare = [filter_value](const auto& e) {
    return e.type() == filter_value;
  };

  auto warn_count =
    std::count_if(std::begin(local), std::end(local), type_compare);

  std::cout << '\n';
  std::cout << "Number of " << Alarm{ filter_value }.as_string()
            << " objects is " << warn_count << '\n';

  auto remove_it =
    std::remove_if(std::begin(local), std::end(local), type_compare);

  std::fill(remove_it,
            std::end(local),
            Alarm{}); // fill end of array with invalid objects

  for (auto& e : local) {
    if (e.type() != Alarm::Type::invalid) { std::cout << e.as_string() << ' '; }
  }
  std::cout << std::endl;
}

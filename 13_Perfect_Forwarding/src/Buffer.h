// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#ifndef TEMPLATE_BUFFER_H
#define TEMPLATE_BUFFER_H

#include "Alarm.h"
#include <array>
#include <cstddef>
#include <optional>

template<typename Ty, std::size_t sz = 16>
class tBuffer {
public:
  tBuffer() = default;

  template<typename U>
  bool add(U&& in); // Perfect-forwarding / Meyer's Universal-reference

  using Type = typename Ty::Type;

  template<typename... Param_Ty>
  bool emplace(Param_Ty&&... param);

  std::optional<Ty>     get();
  bool                  is_empty() const;
  constexpr std::size_t capacity() const { return sz; }
  void                  reset();

private:
  // static constexpr std::size_t sz{ 16 };
  using Container = std::array<Ty, sz>;

  Container   elems{};
  std::size_t write_pos{};
  std::size_t read_pos{};
  std::size_t num_elems{};
};

template<typename Ty, std::size_t sz>
template<typename U>
bool tBuffer<Ty, sz>::add(U&& in) // (2)
{
  if (num_elems == sz) return false;

  elems[write_pos] = std::forward<U>(in);
  ++num_elems;
  ++write_pos;
  if (write_pos == sz) write_pos = 0;

  return true;
}

template<typename Ty, std::size_t sz>
template<typename... Param_Ty>
bool tBuffer<Ty, sz>::emplace(Param_Ty&&... param)
{
  if (num_elems == sz) return false;

  elems[write_pos] = Ty{ std::forward<Param_Ty>(param)... };
  ++num_elems;
  ++write_pos;
  if (write_pos == sz) write_pos = 0;

  return true;
}

template<typename Ty, std::size_t sz>
std::optional<Ty> tBuffer<Ty, sz>::get()
{
  if (num_elems == 0) return std::nullopt;

  auto return_val = std::move(elems[read_pos]); // (2)
  --num_elems;
  ++read_pos;
  if (read_pos == sz) read_pos = 0;

  return return_val;
}

template<typename Ty, std::size_t sz>
bool tBuffer<Ty, sz>::is_empty() const
{
  return (num_elems == 0);
}

template<typename Ty, std::size_t sz>
void tBuffer<Ty, sz>::reset()
{
  Container local{};
  elems.swap(local);
  num_elems = read_pos = write_pos = 0;
}

// template<typename Ty, std::size_t sz>
// bool tBuffer<Ty, sz>::emplace(Type type)
// {
//   if (num_elems == sz) return false;

//   elems[write_pos] = Ty{ type };
//   ++num_elems;
//   ++write_pos;
//   if (write_pos == sz) write_pos = 0;

//   return true;
// }

// template<typename Ty, std::size_t sz>
// bool tBuffer<Ty, sz>::emplace(Type type, const char* str)
// {
//   if (num_elems == sz) return false;

//   elems[write_pos] = Ty{ type, str };
//   ++num_elems;
//   ++write_pos;
//   if (write_pos == sz) write_pos = 0;

//   return true;
// }

#endif // TEMPLATE_BUFFER_H

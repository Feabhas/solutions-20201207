// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#include "Circular_buffer.h"

bool Buffer::add(Buffer::buffer_type&& in) // (2)
{
  if (num_elems == sz) return false;

  elems[write_pos] = std::move(in); // op=(&&)
  ++num_elems;
  ++write_pos;
  if (write_pos == sz) write_pos = 0;

  return true;
}

std::optional<Buffer::buffer_type> Buffer::get()
{
  if (num_elems == 0) return std::nullopt;

  auto return_val = std::move(elems[read_pos]); // (2)
  --num_elems;
  ++read_pos;
  if (read_pos == sz) read_pos = 0;

  // return std::move(return_val); // redundant move - can inhibit copy elision
  return return_val;
}

bool Buffer::is_empty() const
{
  return (num_elems == 0);
}

void Buffer::reset()
{
  Container local{};
  elems.swap(local);
  num_elems = read_pos = write_pos = 0;
}

bool Buffer::emplace(buffer_type::Type type)
{
  if (num_elems == sz) return false;

  elems[write_pos] = buffer_type{ type }; // op=(&&)
  ++num_elems;
  ++write_pos;
  if (write_pos == sz) write_pos = 0;

  return true;
}

bool Buffer::emplace(buffer_type::Type type, const char* str)
{
  if (num_elems == sz) return false;

  elems[write_pos] = buffer_type{ type, str }; // op=(&&)
  ++num_elems;
  ++write_pos;
  if (write_pos == sz) write_pos = 0;

  return true;
}
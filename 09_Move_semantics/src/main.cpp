// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Circular_buffer.h"
#include "Display.h"
#include "Generator.h"

void* operator new(std::size_t sz)
{
  std::clog << "+++ ALLOC : " << sz << " bytes\n";
  return malloc(sz);
}

void operator delete(void* ptr) noexcept
{
  std::clog << "+++ DEALLOC\n";
  return free(ptr);
}

int main(int argc, char** argv)
{
  int run_count = (argc > 1) ? std::stoi(argv[1]) : 1;

  std::cout << "Running for : " << run_count << " cycles"
            << "\n";

  Buffer    pipe{};
  Generator generator{ pipe };
  Display   display{ pipe };

  for (auto i = 0; i < run_count; ++i) {
    generator.execute();
    display.execute();
  }
}
